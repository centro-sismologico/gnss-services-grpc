use std::path::PathBuf;
use std::error::Error;

fn build(paths:&Vec<PathBuf>) -> Result<(), Box<dyn Error>>{
	let base_path = "../gnss-api/";
	let includes:Vec<PathBuf> = vec![base_path.into()];

	match tonic_build::configure().build_server(true).compile(paths, &includes){
		Ok(_)=>{
			println!("Status API generated");
		},
		Err(_e)=>	{
			eprintln!("Status API NOT generated");
		}
	}

	Ok(())
}
/*
This code take the protobuffers from the api created and create the
rust code that links with structures, enums and services.
*/
fn main() ->Result<(), Box<dyn std::error::Error>>{
	let base_path = "../gnss-api/";

	let status_path:PathBuf = [
		base_path, 
		"status", 
		"device.proto"].iter().collect();

	let sources_db_path:PathBuf = [
		base_path, 
		"moebius_db",
		"sources.proto"].iter().collect();

	let dataset_db_path:PathBuf = [
		base_path, 
		"moebius_db",
		"dataset.proto"].iter().collect();

	let paths:Vec<PathBuf> = vec![
		sources_db_path,
		dataset_db_path,
		status_path,
	].into_iter().filter(|p| p.exists()).collect();


	for path in paths.iter() {
		println!("Protobuffer Path {:?}:exists {}", path, path.exists());
	}

	build(&paths).unwrap();

	Ok(())
}
