//use crate::rpc_device_status::device_service_server::{DeviceService
// as RPCDeviceService,DeviceServiceServer};

//use crate::rpc_device_status::{DeviceRequest, DeviceReply};
use crate::rpc_device_status::DeviceReply;
use crate::rpc_device_status::memory_human_info::Scala as RPCScala;
use crate::rpc_device_status::{MemoryHumanInfo as
							   RPCMemoryHumanInfo,Memory as RPCMemory};
use device_status::info::{Device,Scala, Memory, MemoryHumanInfo};
use prost_types::Timestamp;


impl From<Scala> for RPCScala {
	fn from(value:Scala)->Self {
		match value {
			Scala::Bytes=>Self::Bytes,
			Scala::KiloBytes=>Self::KiloBytes,
			Scala::MegaBytes=>Self::MegaBytes,
			Scala::GigaBytes=>Self::GigaBytes,
			Scala::TeraBytes=>Self::TeraBytes,
		}
	}
}

impl From<Memory> for RPCMemory{
	fn from(value:Memory)->Self {
		Self {
			available:value.get_available(),
			free:value.get_free(),
			total: value.get_total()
		}
		}
}

impl From<MemoryHumanInfo> for RPCMemoryHumanInfo{
	fn from(value:MemoryHumanInfo)->Self {
			Self {
				available:value.get_available(),
				percentage:value.get_percentage(),
				scala: value.get_scala() as i32
			}
		}
}

impl From<Device> for DeviceReply {

	fn from(value:Device)->Self {

		let ts = value.get_datetime().timestamp();
		let ts_prot = Timestamp{seconds:ts, nanos:0};
		Self {
			ids: value.get_id(),
			datetime:Some(ts_prot),
			name: value.get_name(),
			host: value.get_host(),
			path: value.get_db_path().into_os_string().into_string().unwrap(),
			memory: Some(value.get_memory().into()),
			memory_human: Some(value.memory_info().into())		
		}

	}
}

//pub type DeviceRequest = DeviceRequest;
//pub type DeviceReply = DeviceReply;

