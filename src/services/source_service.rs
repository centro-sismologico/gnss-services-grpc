
use tonic::async_trait;

use crate::rpc_database::{SourceReply,
						  SourceReplyId, 
						  SourcesDeletedCountReply,
						  SourceRequestById,
						  SourceRequestByCode,
						  SetSourcesCodeRequest,
						  SetSourcesIdsRequest,
						  SourceCreateRequest};
//use crate::rpc_database::SetSourcesCodeRequest;

//use crate::rpc_device_status::{DeviceRequest, DeviceReply};
use crate::rpc_database::source_service_server::SourceService as RPCSourceService;


use tonic::{/*transport::Server, */Request, Response, Status};

use tokio_stream::wrappers::ReceiverStream;
use tokio::sync::mpsc;
use tonic::Code;



/* import  moevius_db elements */


use moebius_db::db::connection::{establish_connection, enum_mode::Mode};
use moebius_db::models::sources::{Source,NewSource};


#[derive(Clone,Debug)]
pub struct SourceService {
	//pool: AsyncPoolPG,//->> add when implemetes with db
	//items: Arc<Mutex<HashMap<u32, DeviceItem>>>,
}


impl  SourceService {
	pub async fn default()->Self{
		Self {}
	}
}



fn string_to_static_str(s: String) -> &'static str {
    Box::leak(s.into_boxed_str())
}
impl From<SourceCreateRequest> for NewSource<'_> {
	fn from(value:SourceCreateRequest)-> Self {
		NewSource::new(
			(string_to_static_str(value.name),
			 string_to_static_str(value.code),
			 string_to_static_str(value.uri),
			 value.active,
			 value.serie,
			 string_to_static_str(value.data_type))
		)
	}
}

/*
Database::Source Service
*/

impl From<Source> for SourceReply{
	fn from(value:Source)-> Self {
		SourceReply {
			id: value.id as u32,
			name: value.name,
			code: value.code,
			uri:value.uri,
			active:value.active,
			serie:value.serie as u32,
			data_type:value.data_type
		}
	}

}

#[async_trait]
impl RPCSourceService for SourceService {

	type GetAllSourcesInfoStream = ReceiverStream<Result<SourceReply, Status>>;
	type ToggleActiveSourceStream = ReceiverStream<Result<SourceReply, Status>>;
	type ToggleActiveSourcesStream = ReceiverStream<Result<SourceReply, Status>>;

	async fn get_all_sources_info(&self, _request:Request<()>) ->
		Result<Response<Self::GetAllSourcesInfoStream>,Status> {

		// tx.send(Ok(source_reply)).await.unwrap();
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::list(&mut conn).await { 
					Ok(sources)=>{		
						let (tx, rx) = mpsc::channel(120);
			
						tokio::spawn(async move {
						 for source in sources.into_iter(){
							 println!("Sending sources->{:?}", source);
							 // from source to sourcereply
							 let source_reply:SourceReply = source.into();
							 tx.send(Ok(source_reply)).await.unwrap();
						 }
						});
						 //  device to rpc-device
						Ok(Response::new(ReceiverStream::new(rx)))						

					},
					Err(_e)=>{
						Err(Status::new(Code::InvalidArgument,	"Source list cannot be fetched"))
					}
				}
			},
			Err(_e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source list cannot generate connection to db"));
			}
		}



		// consultar lista de sources
	}


	async fn get_source_info_by_id(&self,
							 request:Request<SourceRequestById>
	)->Result<Response<SourceReply>,Status>{
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::read(&mut conn, &request.id).await { 
					Ok(source)=>{		
						 //  device to rpc-device
						Ok(Response::new(source.into()))						
					},
					Err(_e)=>{
						Err(Status::new(Code::InvalidArgument,	"Source list cannot be fetched"))
					}
				}
			},
			Err(_e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source list cannot generate connection to db"));
			}
		}
	}


	async fn get_source_info_by_code(&self,
							 request:Request<SourceRequestByCode>
	)->Result<Response<SourceReply>,Status>{
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::read_by_code(&mut conn, &request.code).await { 
					Ok(source)=>{		
						 //  device to rpc-device
						Ok(Response::new(source.into()))						
					},
					Err(_e)=>{
						Err(Status::new(Code::InvalidArgument,	"Source list cannot be fetched"))
					}
				}
			},
			Err(_e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source list cannot generate connection to db"));
			}
		}

	}


	async fn toggle_active_source(&self,
							 request:Request<SourceRequestByCode>
	)->	Result<Response<Self::ToggleActiveSourceStream>,Status> {

		let normal = Mode::normal();
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::toggle_active(&mut conn, &[request.code]).await { 
					Ok(sources)=>{		
						 //  device to rpc-device
						let (tx, rx) = mpsc::channel(120);
						tokio::spawn(async move {
						 for source in sources.into_iter(){
							 // from source to sourcereply
							 let source_reply:SourceReply = source.into();
							 tx.send(Ok(source_reply)).await.unwrap();
						 }
						});
						 //  device to rpc-device
						Ok(Response::new(ReceiverStream::new(rx)))						



					},
					Err(_e)=>{
						Err(Status::new(Code::InvalidArgument,	"Sources cannot be toggled"))
					}
				}
			},
			Err(_e) => {
				return Err(Status::new(Code::InvalidArgument,
					"Source toggle cannot generate connection to db"));
			}
		}

	}

	async fn toggle_active_sources(&self,
							 request:Request<SetSourcesCodeRequest>
	)->	Result<Response<Self::ToggleActiveSourceStream>,Status> {

		let normal = Mode::normal();
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::toggle_active(&mut conn, &request.codes).await { 
					Ok(sources)=>{		
						 //  device to rpc-device
						let (tx, rx) = mpsc::channel(120);
						tokio::spawn(async move {
						 for source in sources.into_iter(){
							 // from source to sourcereply
							 let source_reply:SourceReply = source.into();
							 tx.send(Ok(source_reply)).await.unwrap();
						 }
						});
						 //  device to rpc-device
						Ok(Response::new(ReceiverStream::new(rx)))						



					},
					Err(_e)=>{
						Err(Status::new(Code::InvalidArgument,	"Sources cannot be toggled"))
					}
				}
			},
			Err(_e) => {
				return Err(Status::new(Code::InvalidArgument,
					"Source toggle cannot generate connection to db"));
			}
		}

	}


	async fn create_source(&self,
							 request:Request<SourceCreateRequest>
	)->Result<Response<SourceReplyId>,Status>{
		/*
		SourceCreaterequest -> NewSource
		 */
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::create(&mut conn, &request.into()).await { 
					Ok(source)=>{		
						 //  device to rpc-device
						Ok(Response::new(SourceReplyId{id:source.id}))						
					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	format!("Source cannot be fetched->{:?}",e)))
					}
				}
			},
			Err(_e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source creation cannot generate connection to db"));
			}
		}

	}


	async fn delete_sources(&self,
							 request:Request<SetSourcesIdsRequest>
	)->Result<Response<SourcesDeletedCountReply>,Status>{
		/*
		SourceCreaterequest -> NewSource
		 */
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::delete_by_ids(&mut conn, &request.ids).await { 
					Ok(amount)=>{		
						 //  device to rpc-device
						Ok(Response::new(SourcesDeletedCountReply{count:amount
		as u32}))						
					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	format!("Source cannot be fetched->{:?}",e)))
					}
				}
			},
			Err(_e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source creation cannot generate connection to db"));
			}
		}

	}


}
