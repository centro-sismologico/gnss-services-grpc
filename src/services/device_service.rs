use std::path::{PathBuf,Path};
use tonic::async_trait;


use device_status::info::Device;//,Scala, Memory, MemoryHumanInfo};
use crate::rpc_device_status::{DeviceReply,DeviceRequest,
							   DeviceDeletedReply,
							   DeviceUpdateRequest,
							   DeviceUpdatedReply,
							   DeviceCreateRequest,
							   DeviceCreateReply};



use crate::rpc_device_status::device_service_server::DeviceService as RPCDeviceService;

use tonic::{/*transport::Server, */Request, Response, Status};
use std::collections::HashMap;
use tokio_stream::wrappers::ReceiverStream;
use tokio::sync::mpsc;
use tonic::Code;
use std::sync::{Arc,Mutex};
use dotenv::dotenv;
use std::env;

/* import  moevius_db elements */







#[derive(Debug,Clone)]
pub struct DeviceItem {
	id:u32,
	name:String,
	db_path: PathBuf
}
impl DeviceItem {

	pub fn get_id(&self) -> u32{
		self.id
	}
	
	pub fn get_name(&self) -> String {
		self.name.clone()
	}

	pub fn get_db_path(&self) -> PathBuf {
		self.db_path.clone()
	}

	pub fn new(id:u32, name:&str, db_path:&Path)->Self{
		Self {id, name:name.into(), db_path:db_path.into()}
	}

	pub fn get_measure(&self)-> DeviceReply{
		let device = Device::new(self.id, &self.name, &self.db_path);
		device.into()
	}
}


impl From<DeviceUpdateRequest> for DeviceItem {
	fn from(value:DeviceUpdateRequest)->Self {
		DeviceItem {
			id: value.ids,
			name: value.name,
			db_path: value.path.into()
		}
}
	
}

#[derive(Clone,Debug)]
pub struct DeviceService {
	//pool: AsyncPoolPG,//->> add when implemetes with db
	items: Arc<Mutex<HashMap<u32, DeviceItem>>>,
}


impl  DeviceService {
	pub async fn default()->Self{
		let mutex_map = Mutex::new(HashMap::new());//Cell used to encapsulate and
		//allows mutability
		let arc_map = Arc::new(mutex_map);
		dotenv().ok();
		let _db_url= env::var("DATABASE_URL").unwrap();
		//let pool = init_pool(&db_url).await.unwrap();
		Self {items:arc_map}
	}
}

impl DeviceService {


	pub fn add(&mut self, id:u32, name:&str, db_path:&Path) {
		let mut items = self.items.lock().unwrap();
		items.insert(
			id, 
			DeviceItem::new(id, name, db_path));
	}

	pub fn send_all(&self)-> Vec<DeviceReply>{
		let items = self.items.lock().unwrap();
		items.iter().map(|(_i,device_item)|{
			device_item.get_measure()		
		}).collect()
	}

	pub fn get(&self, id:&u32) -> Option<DeviceItem> {
		let items = self.items.lock().unwrap();
		items.get(id).cloned()
	}

	pub fn delete(&self, id:&u32)->Option<DeviceDeletedReply>{
		let mut items = self.items.lock().unwrap();
		match items.remove_entry(id) {
			Some((k,_v))=>Some(DeviceDeletedReply {ids:k, deleted:true}),
			None =>Some(DeviceDeletedReply {ids:*id, deleted:false}),
		}
	}

	pub fn update(&self, new_data:&DeviceUpdateRequest)->Option<DeviceUpdatedReply>{
		let mut items = self.items.lock().unwrap();
		match items.remove_entry(&new_data.ids) {
			Some((k,_v))=>{
				let item = new_data.clone().into();
				items.insert(k, item);
				Some(DeviceUpdatedReply {ids:k, updated:true})
			},
			None =>Some(DeviceUpdatedReply {ids:new_data.ids, updated:false}),
		}
	}

	pub fn create(&self, new_data:&DeviceCreateRequest)->Option<DeviceCreateReply>{
		let new_id = self.new_id();//get items mutex and release
		let item = DeviceItem::new(new_id, &new_data.name, Path::new(&new_data.path));
		let mut items = self.items.lock().unwrap();
		items.insert(new_id, item);
		Some(DeviceCreateReply {ids:new_id, created:true})
	}

	pub fn new_id(&self)->u32 {
		let items = self.items.lock().unwrap();
		let ids:u32 =items
			.values()
			.fold(u32::MIN,|acc, item_b|acc.max(item_b.get_id()));
		ids + 1
	}


}


#[async_trait]
impl RPCDeviceService for DeviceService {

	type GetAllDeviceInfoStream=ReceiverStream<Result<DeviceReply, Status>>;

	async fn get_all_device_info(
		&self,
		_request:Request<()>
	) -> Result<Response<Self::GetAllDeviceInfoStream>,Status>{
		let (tx, rx) = mpsc::channel(120);
		let vec_device = self.send_all();

		tokio::spawn(async move{
			for device in vec_device.iter(){
				tx.send(Ok(device.clone())).await.unwrap();
			}
		});

		//  device to rpc-device
		Ok(Response::new(ReceiverStream::new(rx)))
		}

	async fn get_memory_info(&self,
							 request:Request<DeviceRequest>
	)->Result<Response<DeviceReply>,Status>{
		let msg = request.into_inner();
		match self.get(&msg.ids) {
			Some(reply)=> Ok(Response::new(reply.get_measure())),
			None => 	Err(Status::new(Code::InvalidArgument, "Ids is invalid"))
		}
	}

/* delete an item device*/

	async fn delete_device_register(&self, 
							 request:Request<DeviceRequest>
	)->Result<Response<DeviceDeletedReply>,Status> {
		let msg = request.into_inner();
		match self.delete(&msg.ids) {
			Some(reply)=> Ok(Response::new(reply)),
			None => 	Err(Status::new(Code::InvalidArgument, "Ids cannot
							 be deleted"))
		}
	}


/* end */


/* update an item device */
	async fn update_device_register(&self, 
							 request:Request<DeviceUpdateRequest>
	)->Result<Response<DeviceUpdatedReply>,Status> {
		let msg = request.into_inner();
		match self.update(&msg) {
			Some(reply)=> Ok(Response::new(reply)),
			None => 	Err(Status::new(Code::InvalidArgument, "Ids cannot
							 be updated"))
		}
	}
/* end */



/* create an item device */
	async fn create_device_register(&self, 
							 request:Request<DeviceCreateRequest>
	)->Result<Response<DeviceCreateReply>,Status> {
		let msg = request.into_inner();

		match self.create(&msg) {
			Some(reply)=> Ok(Response::new(reply)),
			None => 	Err(Status::new(Code::InvalidArgument, "Device cannot
							 be created"))
		}
	}
/* end */

}

