use tonic::async_trait;

use tonic::{Request, Response, Status};
use tokio_stream::wrappers::ReceiverStream;
use tokio::sync::mpsc;


/* import  moevius_db elements */



use crate::rpc_database::{Uuid, DataReply,LastDataRequest, GsofDataRpc, 
						  DeltaDataRequest, data_reply};


use crate::rpc_database::dataset_service_server::{DatasetService as RPCDatasetService}; // is trait


// from moebius-db
use moebius_db::models::dataset::{Data};
use moebius_db::models::sources::{Source};
use moebius_db::db::connection::{establish_connection, enum_mode::Mode};

/*
Data :: objecto en database
NewData :: objeto antes de guardar dato
*/
use prost_types::{Timestamp};

extern crate serde_json;


use gsof_protocol::protocol::gsof::GsofData;

/*
Debe existir un objeto que transforme GsofData -> GsofReply
*/

impl From<GsofData> for GsofDataRpc {
	fn from(value:GsofData) ->  Self {
		GsofDataRpc {
			x: value.ecef.unwrap().x_pos,
			y: value.ecef.unwrap().y_pos,
			z: value.ecef.unwrap().z_pos,
			xx: value.position_vcv.unwrap().xx,
			zz: value.position_vcv.unwrap().zz,
			yy: value.position_vcv.unwrap().yy,
			xy: value.position_vcv.unwrap().xy,
			xz: value.position_vcv.unwrap().xz,
			yz: value.position_vcv.unwrap().zz,
		}
	}
}


/***/
impl From<Data> for DataReply {
	fn from(value:Data)->Self {
		let uuid = Uuid{value:value.id.as_bytes().to_vec()};
		let ts = Timestamp {
			seconds:value.seconds(),
			nanos:value.nanoseconds() as i32};
		// buena conversion: 
		/*
		Obtener source y el tipo.
		Si tipo Gsof _> GsofData
		Tipo Test ->  TestData
		 */
		// value data -> gsodata -> GSofData RPC
		let gsof_value = GsofData::json_from_value(&value.data).unwrap();
		// gsof_value -> encapsular
		let data = data_reply::Data::Gsof(gsof_value.into());
		Self {
			id: Some(uuid),
			dt_gen: Some(ts),
			data: Some(data),
			source_id: value.source_id
		}
	}
}


/*
Implementar servicio
*/

#[derive(Default)]
pub struct DatasetService {
	//pool: AsyncPoolPG,//->> add when implemetes with db
}


use std::error::Error;

impl DatasetService {
	pub async fn last_n_data(&self, sources:&[i32],n:i64) ->
	Result<Vec<Data>, Box<dyn Error>>{
		let normal = Mode::normal();
		// get soruces by id

		//
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Data::list_last_n(&mut conn, sources, n).await { 
					Ok(dataset)=>{		
						 //  device to rpc-device
						Ok(dataset)						
					},
					Err(e)=>{
						return Err(e)
					}
				}
			},
			Err(e) => {
				return Err(e.into())
			}
		}
		
	}
	pub async fn list_between_times(
		&self,
		sources_id:&[i32], 
		between:(DateTime<Utc>, DateTime<Utc>)) -> 	Result<Vec<Data>,Box<dyn Error>>{

		let normal = Mode::normal();
		// get soruces by id
		let mut conn = establish_connection(normal.clone()).await.unwrap();
		let sources = Source::read_list_by_ids(&mut conn, sources_id).await.unwrap();
		//
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Data::list_between(&mut conn, &sources, between).await { 
					Ok(dataset)=>{		
						 //  device to rpc-device
						return Ok(dataset)						
					},
					Err(e)=>{
						return Err(e)
					}
				}
			},
			Err(e) => {
				return Err(e.into())
			}
		}


	}

}

use chrono::{DateTime, TimeZone, Utc};

fn from_ts_to_dt(ts:Timestamp)-> DateTime<Utc> {
	let secs = ts.seconds;
	let nanos = u32::try_from(ts.nanos).unwrap();
	let local_result = Utc.timestamp_opt(secs,nanos).unwrap();
	local_result
}


#[async_trait]
impl RPCDatasetService for DatasetService {

	type GetLastDataStream=ReceiverStream<Result<DataReply, Status>>;
	type GetDataDeltaTimeStream=ReceiverStream<Result<DataReply,Status>>;

	async fn get_last_data(
		&self,
		request: Request<LastDataRequest>
	) -> Result<Response<Self::GetLastDataStream>,Status> {
		/* self es el service y se podria obtener el objeto con el
		 * poool de conexiones*/
		let (tx, rx) = mpsc::channel(120);
		let msg = request.into_inner();
		let sources_id = [msg.source_id];
		let amount = msg.amount.into();
		let vec_device = self.last_n_data(&sources_id, amount).await.unwrap();

		tokio::spawn(async move{
			for device in vec_device.iter(){
				let datareply:DataReply = (device.clone()).into();
				tx.send(Ok(datareply)).await.unwrap();
			}
		});

		//  device to rpc-device
		Ok(Response::new(ReceiverStream::new(rx)))
	}


	 async fn get_data_delta_time(
		 &self,
		 request: Request<DeltaDataRequest>
	 ) -> Result<Response<Self::GetDataDeltaTimeStream>,Status> {
		 /*
		 get_data_delta_time
		  */
		let (tx, rx) = mpsc::channel(1200);
		let msg = request.into_inner();
		let t0 = from_ts_to_dt(msg.dt_start.unwrap());
		let tf = from_ts_to_dt(msg.dt_end.unwrap());


		 // Data::list_betweeen(conn, souces, between)
		let sources_id = [msg.source_id];

		let vec_device = self.list_between_times(&sources_id, (t0,tf)).await.unwrap();

		tokio::spawn(async move{
			for device in vec_device.iter(){
				let datareply:DataReply = (device.clone()).into();
				tx.send(Ok(datareply)).await.unwrap();
			}
		});

		Ok(Response::new(ReceiverStream::new(rx)))

	 }



}
