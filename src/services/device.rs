use std::path::{PathBuf,Path};
use tonic::async_trait;
use device_status::info::Device;//,Scala, Memory, MemoryHumanInfo};
use crate::rpc_device_status::{DeviceReply,DeviceRequest,
							   DeviceDeletedReply,
							   DeviceUpdateRequest,
							   DeviceUpdatedReply,
							   DeviceCreateRequest,
							   DeviceCreateReply};

use crate::rpc_database::{SourceReply,
						  SourceReplyId, 
						  SourcesDeletedCountReply,
						  SourceRequestById,
						  SourceRequestByCode,
						  SetSourcesCodeRequest,
						  SetSourcesIdsRequest,
						  SourceCreateRequest};
//use crate::rpc_database::SetSourcesCodeRequest;

//use crate::rpc_device_status::{DeviceRequest, DeviceReply};
use crate::rpc_device_status::device_service_server::DeviceService as RPCDeviceService;
use crate::rpc_database::source_service_server::SourceService as RPCSourceService;


use tonic::{/*transport::Server, */Request, Response, Status};
use std::collections::HashMap;
use tokio_stream::wrappers::ReceiverStream;
use tokio::sync::mpsc;
use tonic::Code;
use std::sync::{Arc,Mutex};
use dotenv::dotenv;
use std::env;

/* import  moevius_db elements */
use moebius_db::db::connection::pg_pool::{
	PgManager,
	AsyncPoolPG,
	ObjectManager};

use moebius_db::db::connection::{init_pool,establish_connection, enum_mode::Mode};
use moebius_db::models::sources::{Source,NewSource};


#[derive(Debug,Clone)]
pub struct DeviceItem {
	id:u32,
	name:String,
	db_path: PathBuf
}
impl DeviceItem {

	pub fn get_id(&self) -> u32{
		self.id
	}
	
	pub fn get_name(&self) -> String {
		self.name.clone()
	}

	pub fn get_db_path(&self) -> PathBuf {
		self.db_path.clone()
	}

	pub fn new(id:u32, name:&str, db_path:&Path)->Self{
		Self {id, name:name.into(), db_path:db_path.into()}
	}

	pub fn get_measure(&self)-> DeviceReply{
		let device = Device::new(self.id, &self.name, &self.db_path);
		device.into()
	}
}


impl From<DeviceUpdateRequest> for DeviceItem {
	fn from(value:DeviceUpdateRequest)->Self {
		DeviceItem {
			id: value.ids,
			name: value.name,
			db_path: value.path.into()
		}
}
	
}


fn string_to_static_str(s: String) -> &'static str {
    Box::leak(s.into_boxed_str())
}
impl From<SourceCreateRequest> for NewSource<'_> {
	fn from(value:SourceCreateRequest)-> Self {
		NewSource::new(
			(string_to_static_str(value.name),
			 string_to_static_str(value.code),
			 string_to_static_str(value.uri),
			 value.active,
			 value.serie,
			 string_to_static_str(value.data_type))
		)
	}
}

#[derive(Clone,Debug)]
pub struct DeviceService {
	//pool: AsyncPoolPG,//->> add when implemetes with db
	items: Arc<Mutex<HashMap<u32, DeviceItem>>>,
}


impl  DeviceService {
	pub async fn default()->Self{
		let mutex_map = Mutex::new(HashMap::new());//Cell used to encapsulate and
		//allows mutability
		let arc_map = Arc::new(mutex_map);
		dotenv().ok();
		let db_url= env::var("DATABASE_URL").unwrap();
		//let pool = init_pool(&db_url).await.unwrap();
		Self {items:arc_map}
	}
}

impl DeviceService {


	pub fn add(&mut self, id:u32, name:&str, db_path:&Path) {
		let mut items = self.items.lock().unwrap();
		items.insert(
			id, 
			DeviceItem::new(id, name, db_path));
	}

	pub fn send_all(&self)-> Vec<DeviceReply>{
		let items = self.items.lock().unwrap();
		items.iter().map(|(_i,device_item)|{
			device_item.get_measure()		
		}).collect()
	}

	pub fn get(&self, id:&u32) -> Option<DeviceItem> {
		let items = self.items.lock().unwrap();
		items.get(id).cloned()
	}

	pub fn delete(&self, id:&u32)->Option<DeviceDeletedReply>{
		let mut items = self.items.lock().unwrap();
		match items.remove_entry(id) {
			Some((k,_v))=>Some(DeviceDeletedReply {ids:k, deleted:true}),
			None =>Some(DeviceDeletedReply {ids:*id, deleted:false}),
		}
	}

	pub fn update(&self, new_data:&DeviceUpdateRequest)->Option<DeviceUpdatedReply>{
		let mut items = self.items.lock().unwrap();
		match items.remove_entry(&new_data.ids) {
			Some((k,_v))=>{
				let item = new_data.clone().into();
				items.insert(k, item);
				Some(DeviceUpdatedReply {ids:k, updated:true})
			},
			None =>Some(DeviceUpdatedReply {ids:new_data.ids, updated:false}),
		}
	}

	pub fn create(&self, new_data:&DeviceCreateRequest)->Option<DeviceCreateReply>{
		let new_id = self.new_id();//get items mutex and release
		let item = DeviceItem::new(new_id, &new_data.name, Path::new(&new_data.path));
		let mut items = self.items.lock().unwrap();
		items.insert(new_id, item);
		Some(DeviceCreateReply {ids:new_id, created:true})
	}

	pub fn new_id(&self)->u32 {
		let items = self.items.lock().unwrap();
		let ids:u32 =items
			.values()
			.fold(u32::MIN,|acc, item_b|acc.max(item_b.get_id()));
		ids + 1
	}


}


#[async_trait]
impl RPCDeviceService for DeviceService {

	type GetAllDeviceInfoStream=ReceiverStream<Result<DeviceReply, Status>>;

	async fn get_all_device_info(
		&self,
		_request:Request<()>
	) -> Result<Response<Self::GetAllDeviceInfoStream>,Status>{
		let (tx, rx) = mpsc::channel(120);
		let vec_device = self.send_all();

		tokio::spawn(async move{
			for device in vec_device.iter(){
				tx.send(Ok(device.clone())).await.unwrap();
			}
		});

		//  device to rpc-device
		Ok(Response::new(ReceiverStream::new(rx)))
		}

	async fn get_memory_info(&self,
							 request:Request<DeviceRequest>
	)->Result<Response<DeviceReply>,Status>{
		let msg = request.into_inner();
		match self.get(&msg.ids) {
			Some(reply)=> Ok(Response::new(reply.get_measure())),
			None => 	Err(Status::new(Code::InvalidArgument, "Ids is invalid"))
		}
	}

/* delete an item device*/

	async fn delete_device_register(&self, 
							 request:Request<DeviceRequest>
	)->Result<Response<DeviceDeletedReply>,Status> {
		let msg = request.into_inner();
		match self.delete(&msg.ids) {
			Some(reply)=> Ok(Response::new(reply)),
			None => 	Err(Status::new(Code::InvalidArgument, "Ids cannot
							 be deleted"))
		}
	}


/* end */


/* update an item device */
	async fn update_device_register(&self, 
							 request:Request<DeviceUpdateRequest>
	)->Result<Response<DeviceUpdatedReply>,Status> {
		let msg = request.into_inner();
		match self.update(&msg) {
			Some(reply)=> Ok(Response::new(reply)),
			None => 	Err(Status::new(Code::InvalidArgument, "Ids cannot
							 be updated"))
		}
	}
/* end */



/* create an item device */
	async fn create_device_register(&self, 
							 request:Request<DeviceCreateRequest>
	)->Result<Response<DeviceCreateReply>,Status> {
		let msg = request.into_inner();

		match self.create(&msg) {
			Some(reply)=> Ok(Response::new(reply)),
			None => 	Err(Status::new(Code::InvalidArgument, "Device cannot
							 be created"))
		}
	}
/* end */

}


/*
Database::Source Service
*/

impl From<Source> for SourceReply{
	fn from(value:Source)-> Self {
		SourceReply {
			id: value.id as u32,
			name: value.name,
			code: value.code,
			uri:value.uri,
			active:value.active,
			serie:value.serie as u32,
			data_type:value.data_type
		}
	}

}

#[async_trait]
impl RPCSourceService for DeviceService {

	type GetAllSourcesInfoStream = ReceiverStream<Result<SourceReply, Status>>;
	type ToggleActiveSourceStream = ReceiverStream<Result<SourceReply, Status>>;
	type ToggleActiveSourcesStream = ReceiverStream<Result<SourceReply, Status>>;

	async fn get_all_sources_info(&self, _request:Request<()>) ->
		Result<Response<Self::GetAllSourcesInfoStream>,Status> {

		// tx.send(Ok(source_reply)).await.unwrap();
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::list(&mut conn).await { 
					Ok(sources)=>{		
						let (tx, rx) = mpsc::channel(120);
			
						tokio::spawn(async move {
						 for source in sources.into_iter(){
							 println!("Sending sources->{:?}", source);
							 // from source to sourcereply
							 let source_reply:SourceReply = source.into();
							 tx.send(Ok(source_reply)).await.unwrap();
						 }
						});
						 //  device to rpc-device
						Ok(Response::new(ReceiverStream::new(rx)))						

					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	"Source list cannot be fetched"))
					}
				}
			},
			Err(e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source list cannot generate connection to db"));
			}
		}



		// consultar lista de sources
	}


	async fn get_source_info_by_id(&self,
							 request:Request<SourceRequestById>
	)->Result<Response<SourceReply>,Status>{
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::read(&mut conn, &request.id).await { 
					Ok(source)=>{		
						 //  device to rpc-device
						Ok(Response::new(source.into()))						
					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	"Source list cannot be fetched"))
					}
				}
			},
			Err(e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source list cannot generate connection to db"));
			}
		}
	}


	async fn get_source_info_by_code(&self,
							 request:Request<SourceRequestByCode>
	)->Result<Response<SourceReply>,Status>{
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::read_by_code(&mut conn, &request.code).await { 
					Ok(source)=>{		
						 //  device to rpc-device
						Ok(Response::new(source.into()))						
					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	"Source list cannot be fetched"))
					}
				}
			},
			Err(e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source list cannot generate connection to db"));
			}
		}

	}


	async fn toggle_active_source(&self,
							 request:Request<SourceRequestByCode>
	)->	Result<Response<Self::ToggleActiveSourceStream>,Status> {

		let normal = Mode::normal();
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::toggle_active(&mut conn, &[request.code]).await { 
					Ok(sources)=>{		
						 //  device to rpc-device
						let (tx, rx) = mpsc::channel(120);
						tokio::spawn(async move {
						 for source in sources.into_iter(){
							 // from source to sourcereply
							 let source_reply:SourceReply = source.into();
							 tx.send(Ok(source_reply)).await.unwrap();
						 }
						});
						 //  device to rpc-device
						Ok(Response::new(ReceiverStream::new(rx)))						



					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	"Sources cannot be toggled"))
					}
				}
			},
			Err(e) => {
				return Err(Status::new(Code::InvalidArgument,
					"Source toggle cannot generate connection to db"));
			}
		}

	}

	async fn toggle_active_sources(&self,
							 request:Request<SetSourcesCodeRequest>
	)->	Result<Response<Self::ToggleActiveSourceStream>,Status> {

		let normal = Mode::normal();
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::toggle_active(&mut conn, &request.codes).await { 
					Ok(sources)=>{		
						 //  device to rpc-device
						let (tx, rx) = mpsc::channel(120);
						tokio::spawn(async move {
						 for source in sources.into_iter(){
							 // from source to sourcereply
							 let source_reply:SourceReply = source.into();
							 tx.send(Ok(source_reply)).await.unwrap();
						 }
						});
						 //  device to rpc-device
						Ok(Response::new(ReceiverStream::new(rx)))						



					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	"Sources cannot be toggled"))
					}
				}
			},
			Err(e) => {
				return Err(Status::new(Code::InvalidArgument,
					"Source toggle cannot generate connection to db"));
			}
		}

	}


	async fn create_source(&self,
							 request:Request<SourceCreateRequest>
	)->Result<Response<SourceReplyId>,Status>{
		/*
		SourceCreaterequest -> NewSource
		 */
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::create(&mut conn, &request.into()).await { 
					Ok(source)=>{		
						 //  device to rpc-device
						Ok(Response::new(SourceReplyId{id:source.id}))						
					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	format!("Source cannot be fetched->{:?}",e)))
					}
				}
			},
			Err(e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source creation cannot generate connection to db"));
			}
		}

	}


	async fn delete_sources(&self,
							 request:Request<SetSourcesIdsRequest>
	)->Result<Response<SourcesDeletedCountReply>,Status>{
		/*
		SourceCreaterequest -> NewSource
		 */
		println!("Normal mode");
		let normal = Mode::normal();
		println!("Get all sources info");
		let request = request.into_inner();
		match establish_connection(normal).await {
			Ok(mut conn)=> {
				match Source::delete_by_ids(&mut conn, &request.ids).await { 
					Ok(amount)=>{		
						 //  device to rpc-device
						Ok(Response::new(SourcesDeletedCountReply{count:amount
		as u32}))						
					},
					Err(e)=>{
						Err(Status::new(Code::InvalidArgument,	format!("Source cannot be fetched->{:?}",e)))
					}
				}
			},
			Err(e) => {
				return Err(Status::new(Code::InvalidArgument,	"Source creation cannot generate connection to db"));
			}
		}

	}


}
