use gnss_services_rpc::rpc_device_status::device_service_client::DeviceServiceClient;
use	gnss_services_rpc::rpc_device_status::{
	DeviceRequest,
	DeviceUpdateRequest, 
	DeviceCreateRequest};

use gnss_services_rpc::rpc_database::source_service_client::SourceServiceClient;
use gnss_services_rpc::rpc_database::dataset_service_client::DatasetServiceClient;

use gnss_services_rpc::rpc_database::{SetSourcesCodeRequest,SetSourcesIdsRequest,SourceCreateRequest};
use gnss_services_rpc::rpc_database::{DataReply,LastDataRequest, GsofDataRpc, 
						  DeltaDataRequest, data_reply};
use chrono::{NaiveDate, Weekday};

use tonic::Request;

use tonic::transport::Channel;

use clap; // 3.1.6
use clap::Parser;
use std::net::Ipv4Addr;
use std::path::{PathBuf};
use tonic::codec::CompressionEncoding;
use inquire::{error::InquireError, Select, Text, DateSelect};
use std::io::{self, BufRead};

use gnss_services_rpc::rpc_database::{SourceReply,
						  SourceReplyId, 
						  SourcesDeletedCountReply,
						  SourceRequestById,
						  SourceRequestByCode};
//use crate::rpc_database::SetSourcesCodeRequest;
use chrono::Datelike;
use prost_types::Timestamp;

fn naivedt_to_timestamps(dt:&NaiveDate) -> Timestamp {
	// get seconds
	// get nanos
	
	Timestamp { seconds: dt.and_hms_opt(0, 0, 0).unwrap().timestamp(), nanos: 0 }
}


/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
	#[arg(long)]
    host: Ipv4Addr,
    /// Number of times to greet
    #[arg(long, default_value_t = 5000)]
    port: u16,
    #[arg(short, long)]
	settings: PathBuf
}

impl Args {
	pub fn address(&self)->String{
		format!("{}:{}",self.host, self.port)
	} 
}


fn string_to_static_str(s: String) -> &'static str {
    Box::leak(s.into_boxed_str())
}
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {	
	let args = Args::parse();
	let addr = string_to_static_str(format!("http://{}",args.address()));
	let channel = Channel::from_static(addr).connect().await.unwrap();

	let mut client = DeviceServiceClient::new(channel.clone())
		.send_compressed(CompressionEncoding::Gzip)
		.accept_compressed(CompressionEncoding::Gzip);

	let mut source_client = SourceServiceClient::new(channel.clone())
		.send_compressed(CompressionEncoding::Gzip)
		.accept_compressed(CompressionEncoding::Gzip);

	let mut dataset_client = DatasetServiceClient::new(channel)
		.send_compressed(CompressionEncoding::Gzip)
		.accept_compressed(CompressionEncoding::Gzip);

	let request = Request::new(());
	let response = client.get_all_device_info(request).await?;
	let mut stream = response.into_inner(); 
	while let Some(msg) = stream.message().await? {
		println!("Reponse MSG = {:?}", msg);
	}

	let request = Request::new(DeviceRequest {ids:1});
	let response_unique = client.get_memory_info(request).await?;
	println!("Reponse Unique ===> MSG = {:?}", response_unique);
	

	let request = Request::new(DeviceRequest {ids:2});
	let response_unique = client.delete_device_register(request).await?;
	println!("Reponse Delete Unique ===> MSG = {:?}", response_unique);


	let request = Request::new(());
	let response = client.get_all_device_info(request).await?;
	let mut stream = response.into_inner(); 
	while let Some(msg) = stream.message().await? {
		println!("Reponse POST DELETE MSG = {:?}", msg);
	}

	/*
	Updating or create device 
	 */
	let request = Request::new(
		DeviceUpdateRequest {
			ids:1, 
			name: "test_3".into(), 
			path:"/opt/emacs".into()
		});
	let response_update = client.update_device_register(request).await?;
	println!("Reponse Update Unique ===> MSG = {:?}", response_update);


	let request = Request::new(());
	let response = client.get_all_device_info(request).await?;
	let mut stream = response.into_inner(); 
	while let Some(msg) = stream.message().await? {
		println!("Reponse POST UPDATEMSG = {:?}", msg);
	}


	/*
	Create device 
	 */
	let request = Request::new(
		DeviceCreateRequest {
			name: "test_4".into(), 
			path:"/home/david/data/computacionGPU".into()
		});
	let response_create = client.create_device_register(request).await?;
	println!("Reponse Create Unique ===> MSG = {:?}", response_create);


	let request = Request::new(());
	let response = client.get_all_device_info(request).await?;
	let mut stream = response.into_inner(); 
	while let Some(msg) = stream.message().await? {
		println!("Reponse POST CREATE = {:?}", msg);
	}


	println!("Lising source.s....");
	let request = Request::new(());
	let response = source_client.get_all_sources_info(request).await?;
	let mut stream = response.into_inner(); 
	while let Some(msg) = stream.message().await? {
		println!("Reponse SOURCE LIST Item => {:?}", msg);
	}


	println!("Lising Source by ID....");
	let request = Request::new(SourceRequestById{id:1736});
	let response = source_client.get_source_info_by_id(request).await?;
	println!("Reponse Source Info id ===> MSG = {:?}", response);



	println!("Lising Source by CODE....");
	let request = Request::new(SourceRequestByCode{code:"UTAR".into()});
	let response = source_client.get_source_info_by_code(request).await?;
	println!("Reponse Source Info Code ===> MSG = {:?}", response);


	println!("Toggle Active Source by id....");
	let request = Request::new(SourceRequestByCode{code:"UTAR".into()});
	let response = source_client.toggle_active_source(request).await?;
	let mut stream = response.into_inner(); 

	while let Some(msg) = stream.message().await? {
		println!("Reponse SOURCE LIST Toggle => {:?}", msg);
	}



	println!("Toggle Active Sources(plural) by id....");
	let request =	Request::new(
		SetSourcesCodeRequest{
			codes:["UTAR".to_string(), "TEST".to_string()].to_vec()
		});
	let response = source_client.toggle_active_sources(request).await?;
	let mut stream = response.into_inner(); 

	while let Some(msg) = stream.message().await? {
		println!("Reponse SOURCE LIST Toggle => {:?}", msg);
	}

	println!("Create Source by id....");
	let request = Request::new(
		SourceCreateRequest{
			name:"TEST CLIENT".into(),
			code:"TEST_CLIENT".into(),
			uri: "http://localhost".into(),
			active:true, 
			serie:3,
			data_type:"TEST".into()});

	match source_client.create_source(request).await{
		Ok(source_id)=>println!("Created ok {:?}", source_id),
		Err(err)=>{
			eprintln!("Error on create {:?}", err);
		}

	};

	println!("Delete Source");

	let request = Request::new(
		SetSourcesIdsRequest{ids:vec![1742]});

	match source_client.delete_sources(request).await{
		Ok(source_id)=>println!("Deleted ok {:?}", source_id),
		Err(err)=>{
			eprintln!("Error on delete {:?}", err);
		}

	};

	/*

	Abrir loop
	- habilitar menú para acción
	- según cada acción -> recolectar información
	- será necesario crear un ENUM con repr en string
	 */
	use std::convert::TryFrom;

	#[derive(Debug,)]
	enum GNSSAction {
		GetAllSources,
		GetSourceInfoById,
		GetSourceInfoByCode,
		ToggleActiveSource,
		ToggleActiveSources,
		CreateSource,
		DeleteSources,
		LastNData,
		GetDataDeltaTime,
		End
	}

	impl ToString for GNSSAction {
		fn to_string(&self) -> String {
			match self {
				Self::GetAllSources => String::from("GetAllSources"),
				Self::GetSourceInfoById  =>	String::from("GetSourceInfoById"),
				//get_source_info_by_code
				Self::GetSourceInfoByCode  =>
					String::from("GetSourceInfoByCode"),
				//toggle_active_source
				Self::ToggleActiveSource=>String::from("ToggleActiveSource"),
				Self::ToggleActiveSources=>String::from("ToggleActiveSources"),
				Self::CreateSource => String::from("CreateSource"),
				Self::DeleteSources => String::from("DeleteSources"),
				Self::LastNData => String::from("LastNData"),
				Self::GetDataDeltaTime => String::from("GetDataDeltaTime"),
				Self::End => String::from("END")
			}

		}
	}


	impl TryFrom<String> for GNSSAction where  {
		type Error = &'static str; 

		fn try_from(value:String) -> Result<Self, Self::Error>{
			let input: &str = value.as_ref();
			match input {
				"GetAllSources" => Ok(GNSSAction::GetAllSources),
				"GetSourceInfoById" => Ok(GNSSAction::GetSourceInfoById),
				"GetSourceInfoByCode" => Ok(GNSSAction::GetSourceInfoByCode),
				"ToggleActiveSource" => Ok(GNSSAction::ToggleActiveSource),
				"ToggleActiveSources" => Ok(GNSSAction::ToggleActiveSources),
				"CreateSource" => Ok(GNSSAction::CreateSource),
				"DeleteSources" => Ok(GNSSAction::DeleteSources),
				"LastNData" => Ok(GNSSAction::LastNData),
				"GetDataDeltaTime" => Ok(GNSSAction::GetDataDeltaTime),
				"End" => Ok(GNSSAction::End),
				_ => Err("Error on parse GNSS action")
			}

		}
	}


	let mut flag:bool=true;

	while flag {
		let mut action = GNSSAction::GetAllSources;
		let options = vec![
			"GetAllSources",
			"GetSourceInfoById",
			"GetSourceInfoByCode",
			"ToggleActiveSource",
			"ToggleActiveSources",
			"CreateSource",
			"DeleteSources",
			"LastNData",
			"GetDataDeltaTime",
			"End"];

		let ans:Result<&str, InquireError> = Select::new(
			"¿Qué quieres hacer?",options).prompt();

		match ans {
			Ok(choice)=>{
				action = GNSSAction::try_from(String::from(choice))?;
			},
			Err(_)=>println!("Hubo un error, intenta de nuevo")
		};

		match action {
			GNSSAction::GetAllSources => {
				// enviar el mensaje a server
				let request = Request::new(());
				match source_client
					.get_all_sources_info(request)
					.await {
						Ok(response)=>{
							let mut stream = response.into_inner();
							while let Some(source) = stream.message().await? {
									println!(
										"Response from source info {:?}",
										source);
								}

						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
							continue
						}
						
					}

			},
			GNSSAction::GetSourceInfoById  => {
				let id_value = Text::new("Give me a source id value").prompt()?;
				let idn: i32 = id_value.parse().unwrap();
				let source_request = SourceRequestById {id:idn};
				let request = Request::new(source_request);
				match source_client
					.get_source_info_by_id(request)
					.await {
						Ok(response)=>{
							let reply = response.into_inner();
							println!("Created ok {:?}", reply);
						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
							continue
						}
						
					}

			},
			GNSSAction::GetSourceInfoByCode  => {
				let code_value = Text::new("Give me a source id value").prompt()?;
				let source_request = SourceRequestByCode {code:code_value};
				let request = Request::new(source_request);
				match source_client
					.get_source_info_by_code(request)
					.await {
						Ok(response)=>{
							let reply = response.into_inner();
							println!("Created ok {:?}", reply);
						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
							continue
						}
						
					}
			},
			GNSSAction::ToggleActiveSource => {
				let code_value = Text::new("Give me a source id value").prompt()?;
				let source_request = SourceRequestByCode {code:code_value};
				let request = Request::new(source_request);
				match source_client
					.toggle_active_source(request)
					.await {
						Ok(response)=>{
							let mut stream = response.into_inner();

							while let Some(msg) = stream.message().await? {
								println!("Reponse SOURCE LIST Toggle => {:?}", msg);
							}

						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
							continue
						}
						
					}

			},
			GNSSAction::ToggleActiveSources => {
				let mut codes = vec![];
				let mut code = String::new();
				while code!="END" {
					code = Text::new("Give me a source id value").prompt()?;
					if code != "END" {
						codes.push(code.clone());
					}
				}
				let source_request = SetSourcesCodeRequest {codes};
				// aquí enviar stream 
				let request = Request::new(source_request);
				match source_client
					.toggle_active_sources(request)
					.await {
						Ok(response)=>{
							let mut stream = response.into_inner();

							while let Some(msg) = stream.message().await? {
								println!("Reponse SOURCE LIST Toggle => {:?}", msg);
							}

						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
							continue
						}
						
					}


			},
			GNSSAction::CreateSource =>{
				/*
				Get source information
				SourceCreateRequest
				name: string
				code: string
				uri: string
				active: bool
				serie: int32, i32
				data_type: string
				 */
				let activate = vec!["True", "False"];

				let ans:Result<&str, InquireError> = Select::new(
					"¿Está activa?",activate).prompt();

				let active = match ans{
					Ok("True") => true,
					Ok("False") => false,
					_ => false
				};

				let name = Text::new("Give me a source name").prompt()?;


				let uri = Text::new("Give me a source uri").prompt()?;
				let serie_str = Text::new("Give me a source serie").prompt()?;
				let serie: i32 = serie_str.parse().unwrap();

				let data_type = Text::new("Give me a source data type").prompt()?;
				let code = Text::new("Give me a source id active").prompt()?;
				let new_source = SourceCreateRequest {
					name, code, uri,
					active, serie, data_type};
				let request = Request::new(new_source);

				match source_client
					.create_source(request)
					.await {
						Ok(response)=>{
							let reply = response.into_inner();
							println!("Created ok {:?}", reply);
						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
						}
						
					}		

			},
			GNSSAction::DeleteSources  => {
				/*delete_sources*/
				/**/
				println!("todo");

				let mut ids = vec![];
				loop {
					let id_str = Text::new("Give me a source id value").prompt()?;
					if id_str != "END" {
						let id: i32 = id_str.parse().unwrap();
						ids.push(id);
					} else {
						break
					}
				}

				let ids_request = SetSourcesIdsRequest {
					ids};
				let request = Request::new(ids_request);


				match source_client
					.delete_sources(request)
					.await {
						Ok(response)=>{
							let reply = response.into_inner();
							println!("Deleted ok {:?}", reply);
						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
						}
						
					}		


			},
			GNSSAction::LastNData => {
				/* get_last_data */
				let id_str = Text::new("Give me a source id value").prompt()?;
				let source_id: i32 = id_str.parse().unwrap();
				let amount_str = Text::new("Give me a amount of values").prompt()?;
				let amount: i32 = amount_str.parse().unwrap();

				let last_data_request = LastDataRequest {source_id, amount};

				match dataset_client
					.get_last_data(last_data_request)
					.await {
						Ok(response)=>{

							let mut stream = response.into_inner();

							while let Some(msg) = stream.message().await? {
								println!("Last N data => {:?}", msg);
							}

						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
						}
						
					}	

			},
			GNSSAction::GetDataDeltaTime => {
				// DeltaDataRequest source_id, dt_start, dt_end
				let id_str = Text::new("Give me a source id value").prompt()?;
				let source_id: i32 = id_str.parse().unwrap();

				
				// let start_date 
				// let end_date
				// let max_date

				let current_date = chrono::Utc::now();
				let year = current_date.year();
				let month = current_date.month();
				let day = current_date.day();

				let dt_start = DateSelect::new("Select start date")
					.with_starting_date(NaiveDate::from_ymd_opt(year, month-1, day).unwrap())
					.with_min_date(NaiveDate::from_ymd_opt(year, month-2, day).unwrap())
					.with_max_date(NaiveDate::from_ymd_opt(year, month,day).unwrap())
					.with_week_start(Weekday::Mon)
					.with_help_message("Possible flights will be displayed according to the selected date")
					.prompt().unwrap();

				let dt_end = DateSelect::new("Select start date")
					.with_starting_date(NaiveDate::from_ymd_opt(year, month-1, day).unwrap())
					.with_min_date(NaiveDate::from_ymd_opt(year, month-2, day).unwrap())
					.with_max_date(NaiveDate::from_ymd_opt(year, month, day).unwrap())
					.with_week_start(Weekday::Mon)
					.with_help_message("Possible flights will be displayed according to the selected date")
					.prompt().unwrap();




				let request = DeltaDataRequest {
					source_id, 
					dt_start:Some(naivedt_to_timestamps(&dt_start)), 
					dt_end:Some(naivedt_to_timestamps(&dt_end))};

				match dataset_client
					.get_data_delta_time(request)
					.await {
						Ok(response)=>{

							let mut stream = response.into_inner();

							while let Some(msg) = stream.message().await? {
								println!("Last N data => {:?}", msg);
							}

						},
						Err(_)=>{
							println!(
								"No hay respuesta a la información de sources");
						}
						
					}	


			},
			GNSSAction::End => {
				flag=false;
			}
		}

	}


	/*
	Source Service methods:

	- [X] get_all_sources_info


	- [X] get_source_info_by_id


	- [X] get_source_info_by_code


	- [X] toggle_active_source


	- [X] toggle_active_sources


	- [X] create_source

	- [X] delete_sources

	Dataset service

	- [X] last_n_data

	- list_between_time
	
	 */


	Ok(())
}
