use tonic::transport::Server;//, Request, Response, Status};
use gnss_services_rpc::services::device_service::DeviceService;
use gnss_services_rpc::services::source_service::SourceService;
use gnss_services_rpc::services::dataset::DatasetService;

use gnss_services_rpc::rpc_device_status::device_service_server::DeviceServiceServer;
use gnss_services_rpc::rpc_database::source_service_server::SourceServiceServer;
use gnss_services_rpc::rpc_database::dataset_service_server::DatasetServiceServer;

// get_data
use gnss_services_rpc::tools::get_data::read_csv;
use tonic::codec::CompressionEncoding::Gzip;


use clap; // 3.1.6
use clap::Parser;
use std::net::Ipv4Addr;
use std::path::PathBuf;


/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
	#[arg(long)]
    host: Ipv4Addr,

    /// Number of times to greet
    #[arg(long, default_value_t = 5000)]
    port: u16,

    #[arg(short, long)]
	settings: PathBuf
}

impl Args {
	pub fn address(&self)->String{
		format!("{}:{}",self.host, self.port)
	} 
}



//use moebius_db::db::{connection::establish_connection,enum_mode::Mode};

#[tokio::main]
async fn main() -> Result<(),Box<dyn std::error::Error>>{

	let args = Args::parse();
	println!("{:?}", args);
	println!("Address  {:?}", args.address());	


	let mut service = DeviceService::default().await;
	read_csv(&mut service, &args.settings)?;

	let source_service = SourceService::default().await;

	let dataset_service = DatasetService::default();

	tokio::spawn(async move {
		let addr = args.address().parse().unwrap();
		Server::builder().add_service(
			SourceServiceServer::new(source_service)
				.send_compressed(Gzip)
				.accept_compressed(Gzip)
		).add_service(
	 		DeviceServiceServer::new(service)
				.send_compressed(Gzip)
				.accept_compressed(Gzip)
	 	).add_service(
	 		DatasetServiceServer::new(dataset_service)
				.send_compressed(Gzip)
				.accept_compressed(Gzip)
	 	)
		.serve(addr)
		.await.unwrap();
	}).await.unwrap();

	Ok(())
}

