use std::fs::File;
use csv::ReaderBuilder;

use std::error::Error;
use std::path::PathBuf;
use crate::services::device_service::DeviceService;


#[derive(Debug, serde::Deserialize, Eq, PartialEq)]
struct CsvData {
    id: u32,
    name: String,
    path: PathBuf,
}


pub fn read_csv(service:&mut DeviceService, file_path: &PathBuf)  ->
    Result<(), Box<dyn Error>> {
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().has_headers(true).from_reader(file);
	let iter = rdr.deserialize();
    for result in iter {
        let record:CsvData = result?;
        let id: u32 = record.id;
        let name = record.name.as_str();
        let path = record.path;
        service.add(id, name, &path);
    }
		Ok(())
}
