pub mod rpc_device_status {
	tonic::include_proto!("moebius");
}

pub mod rpc_database {
	tonic::include_proto!("database_source");
	tonic::include_proto!("database_dataset");
}


pub mod api_with_rpc;
pub mod services;
pub mod tools;
